class BMI {
private:
    float m_weight;
    float m_height;
    float m_bmi;

public:
    BMI(float weight, float height);
    float calculate();
};
