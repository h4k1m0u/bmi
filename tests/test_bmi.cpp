#include <iostream>
#include <cassert>

#include "bmi.hpp"

int main() {
  BMI bmi(85.0f, 1.85f);
  float index = bmi.calculate();

  return (index == 24.84f);
}
