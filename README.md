# Build project with CMake
```bash
mkdir build && cd build
cmake ..
make
```


# Build with gcc using BMI library
- Create a file `script.cpp` containing the following:

```cpp
#include <iostream>
#include "bmi.hpp"

int main() {
    std::cout << "BMI: " << calculate_bmi(85, 1.85) << std::endl;
}
```

- To build this script, run from the command-line:

```bash
g++ script.cpp -I/home/hakim/repos/bmi/build/include -L/home/hakim/repos/bmi/build/lib -lbmi
```

Don't forget the `-g` option if you want to debug it with `gdb` later.

- To only build the dynamic library into a `*.so` file, then a script depending on it:

```bash
mkdir lib && cd lib
g++ ../src/bmi.cpp -I../include -shared -fPIC -o libbmi.so
mkdir ../bin && cd ../bin
g++ ../src/test.cpp -I../include -lbmi -L../lib -o test
sudo cp ../lib/libbmi.so /usr/lib
./test
```

- Instead of copying the library to `/usr/lib/`, you can hard-code its path in the executable ([source][1]):

```bash
g++ ../src/test.cpp -I../include -lbmi -L../lib -Wl,-rpath=../lib -o test
./test
```

[1]: https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html


# Python binding with Pybind11
CMake was configured to build a python module called `pybmi` exported using PyBind11. So PyBind11 is first built as follows:

```bash
git clone --depth=1 https://github.com/pybind/pybind11.git
cd pybind11
mkdir build && cd build
pip install pytest
cmake ..
make -j 4
sudo make install
```

Then, to build the python module use (MODE is a custom option):

```bash
cmake .. -DMODE=ON
```

Finally, from the python prompt:

```python
import pybmi
pybmi.
```


# PRIVATE, PUBLIC, or INTERFACE
See [this link](https://stackoverflow.com/a/26038443/2228912), for when to use PRIVATE, PUBLIC, and INTERFACE with:

- `target_include_directories`
- `target_link_libraries`


# Unit tests
See this [video tutorial][ctest-video-tutorial] and this [blog post][ctest-tutorial] for how to run unit tests with cmake. Tests are located inside `/tests` folder and are run using:

```console
$ cd build
$ make test
```

or

```console
$ ctest
```

[ctest-video-tutorial]: https://www.youtube.com/watch?v=ZlMbqFcJEzA
[ctest-tutorial]: https://bastian.rieck.me/blog/posts/2017/simple_unit_tests/


# Documentation
Documentation is generated with `doxygen`. A config file (Doxyfile) was initially produced inside the `/doc` folder with:

```console
$ doxygen -g
```

The Doxyfile was customized by setting the following options:

```bash
PROJECT_NAME     = "BMI"
INPUT            = src include
OUTPUT_DIRECTORY = doc
GENERATE_LATEX   = NO
```

Documentation can be generated with cmake inside `doc/html` folder with:

```console
$ make doc
```


# Installation
Produced targets (executables and libraries) and associated headers can be installed in (i.e. copied to) `/usr/local/` with:

```console
$ make install
```

Any third-party script that makes use of `libbmi` would need to be built as follows:

```console
$ g++ script.cpp -I/usr/local/include/bmi -L/usr/local/lib/bmi -lbmi -Wl,-rpath=/usr/local/lib/bmi -o app
```

### Note

- Only if the executable is linked with a dynamic library (not a static one), would it need the [-Wl,][role-of-Wl,] option.
- If it's the case, use `readelf -d <executable` to see the dynamic section of the executable which shows data used during dynamic linking. The rpath should be hard-coded there (see this [elf file format tutorial][readelf-tutorial]).

[role-of-Wl,]: https://stackoverflow.com/a/6562437/2228912
[readelf-tutorial]: https://www.youtube.com/watch?v=nC1U1LJQL8o
