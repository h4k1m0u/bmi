#include <iostream>
#include <cmath>

#include "bmi.hpp"

int main() {
  // read weight and height from user
  float weight, height;
  std::cout << "Enter your weight [kg]: ";
  std::cin >> weight;
  std::cout << "Enter your height [m]: ";
  std::cin >> height;

  // calculate bmi
  BMI bmi(weight, height);
  float index = bmi.calculate();
  std::cout << "Your BMI: " << index << std::endl;
}
