#include <cmath>

#include "bmi.hpp"

/**
 * @param weight  Human's weight
 * @param height  Humans's height
 */
BMI::BMI(float weight, float height):
  m_weight(weight), m_height(height)
{
}

/**
 * Calculate the BMI from the weight and height provided in the constructor
 * @return m_bmi  Calculated BMI
 */
float BMI::calculate() {
    m_bmi = m_weight / (m_height * m_height);

    // round to two decimals
    m_bmi = std::ceil(m_bmi * 100.0) / 100.0;

    return m_bmi;
}
